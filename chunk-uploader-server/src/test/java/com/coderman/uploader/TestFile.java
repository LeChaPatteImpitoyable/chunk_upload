package com.coderman.uploader;
import cn.hutool.core.io.FileUtil;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 使用Java进行文件分块
 *
 * @Author zhangyukang
 * @Date 2021/1/16 12:59
 * @Version 1.0
 **/
public class TestFile {

    private static Logger logger = LoggerFactory.getLogger(TestFile.class);

    @Test
    public void test1(){
        String chunkFolder = "D:\\upload";
        File dir = new File(chunkFolder);
        File[] files = dir.listFiles();
        for (File file : files) {
            logger.info(file.getName());
        }
    }

    @Test
    public void test2() throws IOException {
        String sourceFilePath = "G:\\emuMMC\\SD00\\eMMC\\00";
        //保存块文件的目录
        String chunkFolder = "D:\\upload\\00\\";
        UploadFileUtil.splitFile(sourceFilePath, chunkFolder);
    }

    @Test
    public void test3() throws IOException {
        String mergeFilePath = "G:\\emuMMC\\SD00\\eMMC\\00";
        String chunkFolder = "D:\\upload\\00\\";
        File chunkDir = new File(chunkFolder);
        UploadFileUtil.mergeFile(mergeFilePath, chunkDir);


    }


    @Test
    public void test4() throws IOException {
        String chunkFolder = "D:\\upload\\03\\04\\05\\";
        File file = FileUtil.file(chunkFolder);
        if(file.isFile()){
            logger.error("需要传目录地址");
            return;
        }
        if(!file.exists()){
            FileUtil.mkdir(chunkFolder);
        }

    }

    @Test
    public void test5() {
        String chunkFolder = "D:\\upload\\03\\";
        File file = FileUtil.file(chunkFolder);
        FileUtil.del(file);
    }
}
