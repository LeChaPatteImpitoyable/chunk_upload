package com.coderman.uploader.response;

import java.util.HashMap;

/**
 * @Author zhangyukang
 * @Date 2021/1/16 14:37
 * @Version 1.0
 **/
public class RestApiResponse<T> {

    /**是否成功 */
    private boolean success;
    private boolean result;
    private String message;

    /** 响应数据 */
    private T data;

    public static  <T>RestApiResponse<T> success(T data){
        RestApiResponse<T> result = new RestApiResponse<>();
        result.success=true;
        result.result=true;
        result.data=data;
        return result;
    }

    public static  <T>RestApiResponse<T> success(){
        RestApiResponse<T> result = new RestApiResponse<>();
        result.success=true;
        result.result=true;
        return result;
    }

    public static <T>RestApiResponse<T> error(T data){
        HashMap<String, Object> data1 = (HashMap<String, Object>)data;
        RestApiResponse<T> result = new RestApiResponse<>();
        result.success=false;
        result.result=false;
        result.message=(String) data1.get("errorMsg");
        result.data=data;
        return result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
