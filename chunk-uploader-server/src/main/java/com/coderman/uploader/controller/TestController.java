package com.coderman.uploader.controller;

import com.coderman.uploader.response.RestApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author zhangyukang
 * @Date 2021/1/16 15:14
 * @Version 1.0
 **/
@RestController
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @GetMapping(value = "/error")
    public RestApiResponse<String> error(){
        return RestApiResponse.success();
    }

    @GetMapping(value = "/getRedisInfo")
    public List<Object> getRedisInfo(@RequestParam("key") String key){
        List<Object> values = redisTemplate.opsForHash().values(key);
        return values;
    }
}
